# External Game Communication

An HTTP botnet, but for roblox server instances. Allows for two way external interaction with Roblox game servers from a webpage. Currently can view userlist and chat, and send commands. Only current command is to spawn a screen message, but more can be easily added.